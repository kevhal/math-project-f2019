
from numpy import sqrt
import time

import numpy as np
import scipy.integrate as integrate

import matplotlib.pyplot as plot
import matplotlib.animation as animation

import RungeKuttaFehlberg as rkf

class Orbit:
    """
    
    Orbit Class

    init_state is [t0,x0,vx0,y0,vx0],
    where (x0,y0) is the initial position
    , (vx0,vy0) is the initial velocity
    and t0 is the initial time
    """
    def __init__(self,
                 init_state = [0, 0, 1, 2, 0],
                 G=1,
                 m1=(7.3477e22 / 5.9736e24) * 1,
                 m2=1,
                 rkf54=None): 
        self.GravConst = G
        self.mSol = m1
        self.mPlanet = m2
        self.state = np.asarray(init_state, dtype='float')
        self.rkf54 = rkf54

        print(self.GravConst, self.mSol, self.mPlanet)
    
    def position(self):
        """compute the current x,y positions of the pendulum arms"""
        x = self.state[1]
        y = self.state[3]
        # print(x, y)
        return (x, y)

    def distance(self):
        #Compute the current distance from earth
        x = self.state[1]
        y = self.state[3]

        return sqrt(x**2+y**2)
    
    def energy(self):
        x = self.state[1]
        y = self.state[3]
        vx = self.state[2]
        vy = self.state[4]
        m1 = self.mPlanet
        m2 = self.mSol
        G = self.GravConst
        U=-G*m1*m2/sqrt(x**2+y**2)
        K= m1*(vx**2+vy**2)/2
        return K+U

    def time_elapsed(self):
        return self.state[0]

    def step(self, h):
        """Uses the trapes method to calculate the new state after h seconds."""
        tEnd = self.state[0] + h

        rkf54.setStepLength(0.1)

        while(self.state[0] < tEnd):
            self.state, __ = rkf54.safeStep(self.state, tEnd)

        rkf54.setStepLength(tEnd - self.state[0])
        self.state, __ = rkf54.step(self.state)


apoapsis = 405410E3 #m
avgSpeed = 1.022E3 #m/s
# apoapsisSpeed = 0.9312554178E3 #m/s kalkluert apoapsis hastighet
# apoapsisSpeed = 0.9646879553E3 #m/s ny kalkulert apoapsis hasthighet ved bruk av semi-major axis
apoapsisSpeed = 0.962002018E3 #m/s ny kalkulert apoapsis med semi hovedakse hentet fra wikipedia. (384 399km) 

initialState = [0.0, 0.0, apoapsisSpeed, -apoapsis, 0.0] #t0, x0, vx0, y0, vy0
gravityConstant = 6.67408E-11
earthMass = 5.9736E24
moonMass = 7.3477E22

# W = np.array([0, 1, 0]) # t, y1, y2, y3
W = initialState
h = 0.1 #stepsize
tol = 05e-14 #tolerance
tEnd = 1.0 #t is element of [0-2.0]


def fEarthMoon(Y):
    #Y tar inn alle Wi verdier, samt nåværende steg på index 0
    #Sender alltid 1 som res[0].
    #Index 0 brukes til noe rart og sendes av en eller annen grunn gjennom her

    #t0, x0, vx0, y0, vy0
    
    res = np.ones(5)

    G = 6.67428E-11   #Gravitasjonskonstanten
    M = 5.9736E24      #Massen av jorda

    x = Y[1]                #Månens x-posisjon
    y = Y[3]                #Månens y-posisjon
    Vx = Y[2]               #Månens fart i x-retning
    Vy = Y[4]               #Månens fart i y-retning

    #Bevegelseslikningene. xPrime står for x' etc
    xPrime = Vx                         
    VxPrime = (G*M*(-x))/(x**2+y**2)**(3/2)
    yPrime = Vy
    VyPrime = (G*M*(-y))/(x**2+y**2)**(3/2)


    res[1] = xPrime
    res[2] = VxPrime
    res[3] = yPrime
    res[4] = VyPrime


    return res

rkf54 = rkf.RungeKuttaFehlberg54(fEarthMoon, 5, h, tol) #Functions, dimension of W, stepsize, tolerance

# make an Orbit instance
orbit = Orbit(initialState, gravityConstant, earthMass, moonMass, rkf54)


dt = 1/30 # 60 frames per 
#30 frames = 1 day = 1 second
# = 86400 seconds
# model seconds per frame = 86400 / fps = 86400 / 30 = 2880 seconds
stepSize = 2880 #2880 seconds for every frame - 1 model day = 1 real second
#stepSize = 10000

# The figure is set
fig = plot.figure()

moonRadius = 1738.14E3
xlim = apoapsis*2 + moonRadius*10
ylim = xlim
axes = fig.add_subplot(111, aspect='equal', autoscale_on=True, xlim=(-xlim, xlim), ylim=(-ylim, ylim))


xPosition = []
yPosition = []


line2, = axes.plot([], [], 'o-b', lw=2) # The blue planet
# line1, = axes.plot(xPosition, yPosition, linestyle='--', label='rocket') # Cyan moon orbit
line1, = axes.plot([], [], 'o-c', lw=2) # Cyan moon

time_text = axes.text(0.02, 0.95, '', transform=axes.transAxes)
realtime_text = axes.text(0.02, 0.90, '', transform=axes.transAxes)
distance_text = axes.text(0.02, 0.85, '', transform=axes.transAxes)


def init():
    """initialize animation"""
    line1.set_data([], [])
    line2.set_data([], [])
    time_text.set_text('')
    realtime_text.set_text('')
    distance_text.set_text('')
    # global lastTime
    

    return line1, line2, time_text, realtime_text



lastTime = time.time_ns()
startTime = time.time()

def animate(i):
    """perform animation step"""
    
    global pause
    pause = False
    global orbit, dt, startTime, lastTime, xPosition, yPosition

    secondsPerMoonRound = 2360584.685
    if( orbit.time_elapsed() >= secondsPerMoonRound ):
        pause = True
        print(orbit.state)
    

    if not pause:

        # time0 = time.time_ns()


        elapsedTime = time.time() - startTime

        deltaTime = time.time_ns() - lastTime
        stepSize = deltaTime * 10**(-9) * 86400 

        lastTime = time.time_ns()
        

        orbit.step(stepSize)

        line2.set_data([0.0,0.0])


        # pos = orbit.position()
        # xPosition.append(pos[0])
        # yPosition.append(pos[1])

        # line1.set_data(xPosition, yPosition)
        line1.set_data(*orbit.position())

        time_text.set_text('time = %.1fs | %.1fd' %(orbit.time_elapsed(), (orbit.time_elapsed()/86400)))
        realtime_text.set_text('realtime = %.3fs' % elapsedTime)
        distance_text.set_text('distance = %.1fkm' % (orbit.distance() / 1000))

        # computeTime = time.time_ns() - time0
        # if (computeTime > 5033333):
            # print('compute time exceeded limit. Delta: %.2fns, limit: 6033333ns' %(computeTime))
        # print('computeTime: %.2fns' % computeTime)


        return line1,line2, time_text, realtime_text, distance_text
    else:
        time.sleep(10)
        return line1,line2, time_text, realtime_text, distance_text #TESTTEST

# choose the interval based on dt and the time to animate one step
# Take the time for one call of the animate.
t0 = time.time_ns()
animate(0)
t1 = time.time_ns()

delay = (dt*(10**9)) - (t1 - t0)
print('delay: %.2fns' %delay)
delay = delay*(10**(-6))
print('delay: %.2fms' %delay)
print('dt: %.5f' %dt)
delay = 16 

anim=animation.FuncAnimation(fig,           # figure to plot in
                        animate,            # function that is called on each frame
                        frames=6000,         # total number of frames 
                        interval=delay ,    # time to wait between each frame.
                        repeat=False,
                        blit=True, 
                        init_func=init      # initialization
                        )





# save the animation as an mp4.  This requires ffmpeg or mencoder to be
# installed.  The extra_args ensure that the x264 codec is used, so that
# the video can be embedded in html5.  You may need to adjust this for
# your system: for more information, see
# http://matplotlib.sourceforge.net/api/animation_api.html

anim.save('moonInOrbit.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
# anim.save('myAnimation.gif', writer='imagemagick', fps=30)

plot.show()
