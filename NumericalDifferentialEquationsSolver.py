import numpy as np
import matplotlib.pyplot as plt


def eulersMethod(steps, w0, interval):
    i = 0
    w = w0
    h = (interval[1] - interval[0])/steps
    t = interval[0]
    while i < steps:
        s = f(t, w)
        w = w + h * s
        t += h
        i += 1
    return np.abs(w - y(t))

def eulersMethodLocal(steps, w0, interval):
    i = 0
    w = w0
    h = (interval[1] - interval[0])/steps
    t = interval[0]
    while i < 2:
        s = f(t, w)
        w = w + h * s
        t += h
        i += 1
    return np.abs(w - y(t))


def rk4(steps, w0, interval):
    i = 0
    w = w0
    h = (interval[1] - interval[0]) / steps
    t = interval[0]
    while i < steps:
        s1 = f(t, w)
        s2 = f(t + h /2, w + h*s1/2)
        s3 = f(t + h /2, w + h*s2/2)
        s4 = f(t + h, w + h*s3)
        w = w + (h / 6) * (s1 + 2*s2 + 2*s3 + s4)
        t += h
        i += 1
        return np.abs(w - y(t))

def rk4Local(steps, w0, interval):
    i = 0
    w = w0
    h = (interval[1] - interval[0]) / steps
    t = interval[0]
    while i < 2:
        s1 = f(t, w)
        s2 = f(t + h /2, w + h*s1/2)
        s3 = f(t + h /2, w + h*s2/2)
        s4 = f(t + h, w + h*s3)
        w = w + (h / 6) * (s1 + 2*s2 + 2*s3 + s4)
        t += h
        i += 1
        return np.abs(w - y(t))


def trapezoidalMethod(steps, w0, interval):
    i = 0
    w = w0
    h = (interval[1] - interval[0])/steps
    t = interval[0]
    while i < steps:
        w = w + (h / 2) * (f(t,w) + f(t + h, w + h*f(t, w)))
        t += h
        i += 1
    return np.abs(w - y(t))

def trapezoidalMethodLocal(steps, w0, interval):
    i = 0
    w = w0
    h = (interval[1] - interval[0])/steps
    t = interval[0]
    while i < 2:
        w = w + (h / 2) * (f(t, w) + f(t + h, w + h*f(t, w)))
        t += h
        i += 1
    return np.abs(w - y(t))


def eulersMethodPrint(h, int,y0):
    y_values = []
    x_values = []
    for j in range(len(h)):
        w = []
        e = []
        x = []
        w.append(y0)
        e.append(0)
        x.append(0)
        t = h[j]
        stepsize = t
        i = 1
        while t < int[1]:
            x.append(t)
            s = f(t, w[i-1])
            wi = w[i - 1] + s*stepsize
            w.append(wi)
            e.append(np.abs(wi - y(t)))
            t += stepsize
            i += 1
        y_values.append(w)
        x_values.append(x)

    xs = np.linspace(0, 1, 100)  # 100 linearly spaced numbers
    ys = np.exp(xs**3 / 3)

    saver = plt.figure()
    plt.figure(figsize=(7, 7))
    for i in range(len(x_values)):
        if i == 0:
            plt.plot(x_values[i], y_values[i], color='black', label='h = 0.1')
        elif i == 1:
            plt.plot(x_values[i], y_values[i], color='blue', label='h = 0.05')
        else:
            plt.plot(x_values[i], y_values[i], color='green', label='h = 0.025')
    plt.plot(xs, ys, color='red', label='correct solution')
    plt.legend(loc='best')
    plt.show()
    saver.savefig("eulerMethodExample.pdf", bbox_inches='tight')


def f(t, w):
    return w*t**2


def y(t):
    return np.exp(t**3 / 3)


#eulersMethodPrint([0.1, 0.05, 0.025], [0, 1], 1)
errors = []
errors.append([1/10, trapezoidalMethod(10, 1.0, [0, 1]), eulersMethod(10, 1.0, [0, 1])])
errors.append([1/100, trapezoidalMethod(100, 1.0, [0, 1]), eulersMethod(100, 1.0, [0, 1])])
errors.append([1/1000, trapezoidalMethod(1000, 1.0, [0, 1]), eulersMethod(1000, 1.0, [0, 1])])
for i in range(3):
    print("h = %s, e_t = %s, e_e = %s " %(errors[i][0], errors[i][1], errors[i][2]))
rk4(1000, 1., [0, 1])