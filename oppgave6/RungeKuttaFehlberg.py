#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 20:26:05 2018

@author: rivertz
"""

import numpy as np
import math as m
import sys


class RungeKuttaFehlberg54:
    A = np.array(
        [[0,    0,    0,    0,  0, 0],
         [1/4,    0,    0,    0,  0, 0],
         [3/32,    9/32,    0,    0,  0, 0],
         [1932/2197, -7200/2197, 7296/2197,    0,  0, 0],
         [439/216,   -8, 3680/513, -845/4104,  0, 0],
         [-8/27,    2, -3544/2565, 1859/4104, -11/40, 0]])

    B = np.array(
        [[25/216,    0, 1408/2565, 2197/4104, -1/5, 0],
         [16/135,    0, 6656/12825, 28561/56430, -9/50, 2/55]])

    def __init__(self, function, dimension, stepsize, tolerance):
        self.F = function
        self.dim = dimension
        self.h = stepsize
        self.tol = tolerance
        print(self.dim, self.h, self.tol)

    def step(self, Win):
        s = np.zeros((6, self.dim))

        # print(Win)

        for i in range(0, 6):
            s[i, :] = self.F(Win+self.h*self.A[i, 0:i].dot(s[0:i, :]))

        Zout = Win+self.h*(self.B[0, :].dot(s))
        Wout = Win+self.h*(self.B[1, :].dot(s))

        E = np.linalg.norm(Wout-Zout, 2)/np.linalg.norm(Wout, 2)
        return Wout, E

    def safeStep(self, Win, tEnd):
        self.tEnd = tEnd

        Wout, E = self.step(Win)
        # Check if the error is tolerable
        if(not self.isErrorTolerated(E)):
            # Try to adjust the optimal step length
            self.adjustStep(E)
            Wout, E = self.step(Win)
        # If the error is still not tolerable
        counter = 0
        while(not self.isErrorTolerated(E)):
            # Try if dividing the steplength with 2 helps.
            self.divideStepByTwo()
            Wout, E = self.step(Win)
            counter = counter + 1
            if(counter > 10):
                print("Error not tolerated. Quitting")
                sys.exit(-1)

        self.adjustStep(E)

        return Wout, E

    def isErrorTolerated(self, E):
        return E < self.tol

    def adjustStep(self, E):
        if(E == 0):
            s = 2
        else:
            s = m.pow(self.tol*self.h/(2*E), 0.25)
            # print("Adjusting with s:", s)
        self.h = s * self.h

        # if(self.h > self.tEnd):
        #     print("manual stepsize")
        #     self.h = self.tEnd / 10

    def divideStepByTwo(self):
        self.h = self.h/2

    def setStepLength(self, stepLength):
        self.h = stepLength


def F(Y):
    M = np.array([[0.49119653, 0.32513304, 0.98057799],
                  [0.20768544, 0.97699416, 0.18220559],
                  [0.96407071, 0.18373237, 0.95307793]])
    res = np.ones(4)
    res[1:4] = M.dot(Y[1:4])
    return res

def F2(Y):
    M = np.array([[ 1, 1],
                  [-1, 1]
                 ])
    res = np.ones(3)
    res[1:3] = M.dot(Y[1:3])
    return res

def fEarthMoon(Y):
    #Y tar inn alle Wi verdier, samt nåværende steg på index 0
    #Sender alltid 1 som res[0].
    #Index 0 brukes til noe rart og sendes av en eller annen grunn gjennom her
    
    res = np.ones(5)

    G = 6.67428*10**(-11)   #Gravitasjonskonstanten
    M = 5.9736*10**24       #Massen av jorda

    x = Y[1]                #Månens x-posisjon
    y = Y[3]                #Månens y-posisjon
    Vx = Y[2]               #Månens fart i x-retning
    Vy = Y[4]               #Månens fart i y-retning

    #Bevegelseslikningene. xPrime står for x' etc
    xPrime = Vx                         
    VxPrime = (G*M*x)/(x**2+y**2)**(3/2)
    yPrime = Vy
    VyPrime = (G*M*y)/(x**2+y**2)**(3/2)


    res[1] = xPrime
    res[2] = VxPrime
    res[3] = yPrime
    res[4] = VyPrime

    return res


def main():

    W = np.array([0, 1, 0]) # t, y1, y2, y3
    h = 0.1 #stepsize
    tol = 05e-14 #tolerance
    tEnd = 1.0 #t is element of [0-2.0]

    rkf54 = RungeKuttaFehlberg54(F2, 3, h, tol) #Functions, dimension of W, stepsize, tolerance

    while(W[0] < tEnd):
        W, E = rkf54.safeStep(W)

    rkf54.setStepLength(tEnd-W[0])
    W, E = rkf54.step(W)

    print(W, E)

    # W = np.array([0, 1, 1, 1]) # t, y1, y2, y3
    # h = 0.1 #stepsize
    # tol = 05e-14 #tolerance
    # tEnd = 2.0 #t is element of [0-2.0]

    # rkf54 = RungeKuttaFehlberg54(F, 4, h, tol) #Functions, dimension of W, stepsize, tolerance

    # while(W[0] < tEnd):
    #     W, E = rkf54.safeStep(W)

    # rkf54.setStepLength(tEnd-W[0])
    # W, E = rkf54.step(W)

    # print(W, E)


if __name__ == "__main__":
    # execute only if run as a script
    main()
