
from numpy import sqrt
import time

import numpy as np
import scipy.integrate as integrate

import matplotlib.pyplot as plot
import matplotlib.animation as animation
import math
from orbitObject import Orbit

apoapsis = 405410E3 #m
avgSpeed = 1.022E3 #m/s

poleRadius = 6356.78E3 #m

avgRadius = 6371E3

apoapsisSpeed = 0.962002018E3 #m/s ny kalkulert apoapsis med semi hovedakse hentet fra wikipedia. (384 399km) 

initialState = [0.0, 0.0, 1E-5, avgRadius, 0.0] #t0, x0, vx0, y0, vy0 # rocket
gravityConstant = 6.67408E-11
earthMass = 5.9736E24
objectMass = 2280E3
angle = 90 # initial angle from x-axis
secondsModelTimePerRealSecond = 600 # number of seconds in model per real second

tol = 05e-16 #tolerance used by Runge-Kutta-Fehlberg

# make an Orbit instance
# Declare orbit object
orbit = None
xPosition = []
yPosition = []
lastTime = time.time_ns()
startTime = time.time()



#animate called repetitively by 
def animate():
    global orbit, startTime, lastTime, xPosition, yPosition

    # elapsedTime = time.time() - startTime   #Elapsed real time

    deltaTime = time.time_ns() - lastTime   #Time between each animate call, used for scaling model time vs real time
    lastTime = time.time_ns()               #Used to calculate deltaTime

    #Use real calculation time and apply modeltime factor
    stepSize = deltaTime * 10**(-9) * secondsModelTimePerRealSecond

    #Calculate next values
    orbit.step(stepSize)

    pos = orbit.position()
    xPosition.append(pos[0])
    yPosition.append(pos[1])

def newOrbit(angle):
    global orbit, lastTime, startTime, xPosition, yPosition

    #Reset values to prepare for next orbit
    xPosition = []
    yPosition = []
    lastTime = time.time_ns()
    startTime = time.time()

    orbit = Orbit(initialState, gravityConstant, earthMass, objectMass, None, angle, 0.1, tol) #inintiate Orbit instance

    n = 6000
    t0 = time.time_ns()
    prevProgress = 0
    abortTime = 0
    for i in range(n):
        progress = i * 100/n

        if(progress - prevProgress > 2):
            abortTime = 0
            prevProgress = progress
            print("%.2f%%" % (progress), end='\r')

        if(abortTime > 5):
            print('Rocket crash at %.2f%%' % (progress))
            break

        
        deltaTime = time.time_ns() - t0
        t0 = time.time_ns()
        abortTime = abortTime + deltaTime*10**(-9)


        animate()
        time.sleep(1E-3)

    return xPosition, yPosition


anglesLaunch1 = [90, 80, 70, 50, 30]
anglesLaunch2 = [90, 90, 70, 50, 30]
anglesLaunch3 = [60, 70, 30, 10, 0]
anglesLaunch4 = [90, 80, 30, 10, 0]

angles = [anglesLaunch1, anglesLaunch2, anglesLaunch3, anglesLaunch4]
graphs = []

# The figure is set
fig = plot.figure(figsize=(10, 10), dpi=80)

xlim = 12000E3
ylim = xlim
axes = fig.add_subplot(111, aspect='equal', autoscale_on=True, xlim=(-xlim, xlim), ylim=(-ylim, ylim))

axes.plot(0, 0, 'o-b', lw=2, markerSize=288) # The blue planet

for angle in angles:
    coordinates = newOrbit(angle)
    graphs.append(coordinates)


colors = ['blue', 'red', 'green', 'yellow']
i = 0
for graph in graphs:
    axes.plot(graph[0], graph[1], linestyle='--', label=str(angles[i]), color=colors[i]) # Cyan rocket
    i = i+1

axes.legend()

plot.show()



# save the animation as an mp4.  This requires ffmpeg or mencoder to be
# installed.  The extra_args ensure that the x264 codec is used, so that
# the video can be embedded in html5.  You may need to adjust this for
# your system: for more information, see
# http://matplotlib.sourceforge.net/api/animation_api.html

# anim.save('orbit.mp4', fps=30, extra_args=['-vcodec', 'libx264'])


