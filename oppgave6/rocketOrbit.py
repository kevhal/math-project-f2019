
from numpy import sqrt
import time

import numpy as np
import scipy.integrate as integrate

import matplotlib.pyplot as plot
import matplotlib.animation as animation
import math
from orbitObject import Orbit

apoapsis = 405410E3 #m
avgSpeed = 1.022E3 #m/s

poleRadius = 6356.78E3 #m

avgRadius = 6371E3

# apoapsisSpeed = 0.9312554178E3 #m/s kalkluert apoapsis hastighet
# apoapsisSpeed = 0.9646879553E3 #m/s ny kalkulert apoapsis hasthighet ved bruk av semi-major axis
apoapsisSpeed = 0.962002018E3 #m/s ny kalkulert apoapsis med semi hovedakse hentet fra wikipedia. (384 399km) 

initialState = [0.0, 0.0, 1E-5, avgRadius, 0.0] #t0, x0, vx0, y0, vy0 # rocket
gravityConstant = 6.67408E-11
earthMass = 5.9736E24
objectMass = 2280E3

angle = 90 # initial angle from x-axis
secondsModelTimePerRealSecond = 600 # number of seconds in model per real second

tol = 05e-16 #tolerance used by Runge-Kutta-Fehlberg

# make an Orbit instance
orbit = Orbit(initialState, gravityConstant, earthMass, objectMass, None, [90, 80, 30, 10, 0], 0.1, tol)

# The figure is set
fig = plot.figure(figsize=(10, 10), dpi=80)

xlim = 12000E3
ylim = xlim
axes = fig.add_subplot(111, aspect='equal', autoscale_on=True, xlim=(-xlim, xlim), ylim=(-ylim, ylim))

xPosition = []
yPosition = []

line2, = axes.plot([], [], 'o-b', lw=2, markerSize=288) # The blue planet
# line2, = axis2.plot([0, 2000000], [900, 1100], color='green', linestyle='-', label='hastighet') # speed
line1, = axes.plot(xPosition, yPosition, linestyle='--', label='rocket') # Cyan rocket

axes.legend()

time_text = axes.text(0.02, 0.95, '', transform=axes.transAxes)
realtime_text = axes.text(0.02, 0.90, '', transform=axes.transAxes)
distance_text = axes.text(0.02, 0.85, '', transform=axes.transAxes)
altitude_text = axes.text(0.02, 0.80, '', transform=axes.transAxes)
speed_text = axes.text(0.02, 0.75, '', transform=axes.transAxes)


lastTime = time.time_ns()
startTime = time.time()

def init():
    """initialize animation"""
    line1.set_data([], [])
    line2.set_data([], [])
    time_text.set_text('')
    realtime_text.set_text('')
    distance_text.set_text('')
    altitude_text.set_text('')
    speed_text.set_text('')

    #Initiate startTime and first lastTime used to calculate deltaTime in animate()
    global lastTime, startTime
    lastTime = time.time_ns()
    startTime = time.time()
    

    return line2, line1, time_text, realtime_text, distance_text, altitude_text, speed_text

#animate called repetitively by 
def animate(i):
    global orbit, startTime, lastTime, xPosition, yPosition

    elapsedTime = time.time() - startTime   #Elapsed real time

    deltaTime = time.time_ns() - lastTime   #Time between each animate call, used for scaling model time vs real time
    lastTime = time.time_ns()               #Used to calculate deltaTime

    #Use real calculation time and apply modeltime factor
    stepSize = deltaTime * 10**(-9) * secondsModelTimePerRealSecond

    #Calculate next values
    orbit.step(stepSize)

    pos = orbit.position()
    xPosition.append(pos[0])
    yPosition.append(pos[1])


    line1.set_data(xPosition, yPosition)   #Draw satelite
    line2.set_data([0.0,0.0])           #Draw earth

    time_text.set_text('time = %.1fs | %.1fd' %(orbit.time_elapsed(), (orbit.time_elapsed()/86400)))
    realtime_text.set_text('realtime = %.3fs' % elapsedTime)
    distance_text.set_text('distance = %.1fkm' % (orbit.distance() / 1000))
    altitude_text.set_text('alt = %.1fkm' % (orbit.altitude() / 1000))
    speed_text.set_text('speed = %.1fm/s' % orbit.speed())


    return line2, line1, time_text, realtime_text, distance_text, altitude_text, speed_text

delay = 1 #Using floating FPS. Delay irrelevant.

anim=animation.FuncAnimation(fig,           # figure to plot in
                        animate,            # function that is called on each frame
                        frames=9900000,     # total number of frames 
                        interval=delay ,    # time to wait between each frame.
                        repeat=False,
                        blit=True, 
                        init_func=init      # initialization
                        )





# save the animation as an mp4.  This requires ffmpeg or mencoder to be
# installed.  The extra_args ensure that the x264 codec is used, so that
# the video can be embedded in html5.  You may need to adjust this for
# your system: for more information, see
# http://matplotlib.sourceforge.net/api/animation_api.html

# anim.save('orbit.mp4', fps=30, extra_args=['-vcodec', 'libx264'])

plot.show()
