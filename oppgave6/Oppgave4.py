import numpy as np

height_offset = 12713.56e3 / 2


# def mass_of_rocket(t):
#     m1 = 2280e3
#     m2 = 480e3
#     m3 = 123e3
#     final_mass = 123000 - (109000*(5/17))
#     if 0 <= t < 168:
#         m = m1 + m2 + m3 + mdot(t) * t
#     elif 168 <= t < 528:
#         m = m2 + m3 + mdot(t) * (t - 168)
#     elif 528 <= t < 678:
#         m = m3 + mdot(t) * (t - 528)
#     else:
#         m = final_mass
#     return m

def mass_of_rocket(t):
    m1 = 2280e3
    m2 = 480e3
    m3 = 123e3
    final_mass = 123000 - (109000*(0.33))
    if 0 <= t < 168:
        m = m1 + m2 + m3 + mdot(t) * t
    elif 168 <= t < 528:
        m = m2 + m3 + mdot(t) * (t - 168)
    elif 528 <= t < 678:
        m = m3 + mdot(t) * (t - 528)
    else:
        m = final_mass
    return m


def area_rocket(t):
    if 0 <= t < 168:
        a = np.pi * (10 / 2) ** 2
    elif 168 <= t < 528:
        a = np.pi * (10 / 2) ** 2
    else:
        a = np.pi * (6.604 / 2) ** 2
    return a

#fuel consumption - mengden vekt som er borte
# def mdot(t):
#     if 0 <= t < 168:
#         mdott = (2300000 - 131000) / 168
#     elif 168 <= t < 528:
#         mdott = 443000 / (528 - 168)
#     elif 528 <= t < 678:
#         mdott = (109000*(5/17)) / (678 - 528)
#     else:
#         mdott = 0
#     return -mdott

def mdot(t):
    if 0 <= t < 168:
        mdott = (2290000 - 131000) / 168
    elif 168 <= t < 528:
        mdott = 443000 / (528 - 168)
    elif 528 <= t < 693:
        mdott = (109000*(5/17)) / (693 - 528)
    else:
        mdott = 0
    return -mdott

# skyvekraft
def f_rocket(t):
    # f1 = 33000e3
    f1 = 35100E3
    # f2 = 4400e3
    f2 = 5141E3
    # f2 = 6000e3
    # f3 = 890000
    f3 = 1033.1E3
    # f3 = 1400000
    if 0 <= t < 168:
        f = f1
    elif 168 <= t < 528:
        f = f2
    # elif 528 <= t < 678:
    elif 528 <= t < 693:
        f = f3
    else:
        f = 0

    return f

# # Temperatur
# def temp(h):
#     if 0 <= h < 11000:
#         t = 288.19 - 0.00649 * h
#     elif 11000 <= h < 25000:
#         t = 216.69
#     else:
#         t = 141.94 + 0.00299 * h
#     return t

# # atmosfærisk trykk
# def get_pressure(h):
#     if 0 <= h < 11000:
#         pressure = 101.29e3 * (temp(h) / 288.06) ** 5.256
#     elif 11000 <= h < 25000:
#         pressure = 127.76e3 * np.exp(-(0.000157) * h)
#     elif 25000 <= h:
#         pressure = 2.488e3 * (temp(h) / 216.6) ** (-11.388)
#     else:
#         pressure = 0
#     return pressure

# Lufttettheten
def get_rho(h):
    # if 0 <= h < 11000:
    h = abs(h)
    if h < 11000:
        t = 15.04 - .00649 * h
        p = 101.29 * ((t + 273.1) / 288.08) ** 5.256
    elif 11000 <= h < 25000:
        t = -56.56
        p = 22.65 * np.exp(1.73 - .000157 * h)
    elif 25000 <= h:
        t = -131.21 + .00299 * h
        p = 2.488 * ((t + 273.1) / 216.6) ** -11.388
    # print('LOCAL P: %.10f' % p)
    return p / (.2869 * (t + 273.1))

# Luftmotstand
def f_d(t, h, v):
    # height_offset = 12713.56e3 / 2
    # h = h - height_offset
    c_d = 0.521
    rho = get_rho(h)
    a = area_rocket(t)
    f_d = -(1 / 2) * c_d * rho * a * (v ** 2)
    #print("rho, c_d, a, v : ", [rho, c_d, a, v])
    return f_d
