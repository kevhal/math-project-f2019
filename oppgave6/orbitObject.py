import numpy as np
import math
from numpy import sqrt

import Oppgave4 as forces

import RungeKuttaFehlberg as rkf

class Orbit:
    """
    
    Orbit Class

    init_state is [t0,x0,vx0,y0,vx0],
    where (x0,y0) is the initial position
    , (vx0,vy0) is the initial velocity
    and t0 is the initial time
    """
    def __init__(self, init_state = [0, 0, 1, 2, 0], G=1, m1=(7.3477e22 / 5.9736e24) * 1, m2=1, rkf54=None, angles=[90, 90, 90, 90, 90], h=0.1, tol=5E-14): 
        self.GravConst = G
        self.mSol = m1
        self.mPlanet = m2
        self.state = np.asarray(init_state, dtype='float')
        # self.rkf54 = rkf54
        self.angle = math.radians(angles[0])
        self.angles = angles

        self.rkf54 = rkf.RungeKuttaFehlberg54(self.fEarthObject, 5, h, tol) #Functions, dimension of W, stepsize, tolerance

        print(self.GravConst, self.mSol, self.mPlanet)

    def getState(self):
        return self.state
    
    def position(self):
        """compute the current x,y positions of the pendulum arms"""
        x = self.state[1]
        y = self.state[3]
        # print(x, y)
        return (x, y)

    def distance(self):
        #Compute the current distance from earth
        x = self.state[1]
        y = self.state[3]

        return sqrt(x**2+y**2)

    def altitude(self):
        return self.distance() - 6371E3

    def speed(self):
        vx = self.state[2]
        vy = self.state[4]
        return sqrt(vx**2 + vy**2)
    
    def energy(self):
        x = self.state[1]
        y = self.state[3]
        vx = self.state[2]
        vy = self.state[4]
        m1 = self.mPlanet
        m2 = self.mSol
        G = self.GravConst
        U=-G*m1*m2/sqrt(x**2+y**2)
        K= m1*(vx**2+vy**2)/2
        return K+U

    def time_elapsed(self):
        return self.state[0]

    def step(self, h):
        """Uses the trapes method to calculate the new state after h seconds."""
        tEnd = self.state[0] + h

        rkf54 = self.rkf54

        rkf54.setStepLength(0.1)


        while(self.state[0] < tEnd):
            modelTimeElapsed = self.time_elapsed()
            if(12 < modelTimeElapsed < 20):
                self.angle = math.radians(self.angles[1])
            elif (100 < modelTimeElapsed < 115):
                self.angle = math.radians(self.angles[2])
            elif (280 < modelTimeElapsed < 315):
                self.angle = math.radians(self.angles[3])
            elif (450 < modelTimeElapsed < 500):
                self.angle = math.radians(self.angles[4])

            self.state, __ = rkf54.safeStep(self.state, tEnd)

        rkf54.setStepLength(tEnd - self.state[0])
        self.state, __ = rkf54.step(self.state)
    
    #Gravity acceleration
    def gx(self, Y):
        G = 6.67428E-11   #Gravitasjonskonstanten
        M = 5.9736E24      #Massen av jorda

        x = Y[1]                #Månens x-posisjon
        y = Y[3]                #Månens y-posisjon

        return (G*M*(-x))/(x**2+y**2)**(3/2)

    #Gravity acceleration
    def gy(self, Y):
        G = 6.67428E-11   #Gravitasjonskonstanten
        M = 5.9736E24      #Massen av jorda

        x = Y[1]                #Månens x-posisjon
        y = Y[3]                #Månens y-posisjon

        return (G*M*(-y))/(x**2+y**2)**(3/2)

    #Gravity force
    def Gx(self, Y):
        return self.gx(Y) * forces.mass_of_rocket(Y[0])

    #Gravity force
    def Gy(self, Y):
        return self.gy(Y) * forces.mass_of_rocket(Y[0])

    #Force of rocket engine factored with rockets orientation
    def FrocketX(self, Y):
        return forces.f_rocket(Y[0]) * np.cos(self.angle)

    #Force of rocket engine factored with rockets orientation
    def FrocketY(self, Y):
        return forces.f_rocket(Y[0]) * np.sin(self.angle)

    #Force of air resistance factored with rockets velocity direction
    def FairResitanceX(self, Y):
        x = Y[1]
        y = Y[3]
        vx = Y[2]
        vy = Y[4]
        v = sqrt(vx**2 + vy**2) #Get total speed
        altitude = sqrt(x**2 + y**2) - 6371E3 #m #Get altitude above sea level
        if (vx == 0):
            return 0
        else:
            return forces.f_d(Y[0], altitude, v) * (vx / v) # Return force from air resistance x-factor

    #Force of air resistance factored with rockets velocity direction
    def FairResitanceY(self, Y):
        x = Y[1]
        y = Y[3]
        vx = Y[2]
        vy = Y[4]
        v = sqrt(vx**2 + vy**2) #Get total speed
        altitude = sqrt(x**2 + y**2) - 6371E3 #m #Get altitude above sea level
        if (vy == 0):
            return 0
        else:
            return forces.f_d(Y[0], altitude, v) * (vy / v) # Return force from air resistance x-factor

    def fEarthObject(self, Y):
        res = np.ones(5)

        #Y = [t0, x0, vx0, y0, vy0]
        Vx = Y[2]               #Månens fart i x-retning
        Vy = Y[4]               #Månens fart i y-retning


        #x-direction forces
        GravX = self.Gx(Y)                   #Force of graviy
        FxRocket = self.FrocketX(Y)          #Rocket engine force
        FxAir = self.FairResitanceX(Y)       #Force of air resistance

        #y-direction forces
        GravY = self.Gy(Y)                   #Force of gravity
        FyRocket = self.FrocketY(Y)          #Rocket engine force
        FyAir = self.FairResitanceY(Y)       #Force of air resistance

        #Bevegelseslikningene. xPrime står for x' etc
        xPrime = Vx                         
        VxPrime = (GravX + FxRocket +  FxAir) / forces.mass_of_rocket(Y[0])
        yPrime = Vy
        VyPrime = (GravY + FyRocket + FyAir ) / forces.mass_of_rocket(Y[0])

        res[1] = xPrime
        res[2] = VxPrime
        res[3] = yPrime
        res[4] = VyPrime


        return res