import Oppgave4 as o
from numpy import sqrt
import time
import numpy as np
import math as m
import sys
import matplotlib.pyplot as plot
import matplotlib.animation as animation


class Rocket_Simulation:
    # From butchertableau
    A = np.array(
        [[0, 0, 0, 0, 0, 0],
         [1 / 4, 0, 0, 0, 0, 0],
         [3 / 32, 9 / 32, 0, 0, 0, 0],
         [1932 / 2197, -7200 / 2197, 7296 / 2197, 0, 0, 0],
         [439 / 216, -8, 3680 / 513, -845 / 4104, 0, 0],
         [-8 / 27, 2, -3544 / 2565, 1859 / 4104, -11 / 40, 0]])

    B = np.array(
        [[25 / 216, 0, 1408 / 2565, 2197 / 4104, -1 / 5, 0],
         [16 / 135, 0, 6656 / 12825, 28561 / 56430, -9 / 50, 2 / 55]])

    def __init__(self,
                 tolerance,
                 h,
                 init_state=[0, 0, 0, 0, 0],
                 G=6.67430e-11,
                 m_earth=5.9736e24):
        self.GravConst = G
        self.tol = tolerance
        self.dim = len(init_state)
        self.m_earth = m_earth
        self.state = np.asarray(init_state, dtype='float')
        self.h = h
        self.originalH = self.h

    #gets the current position of the rocket
    def position(self):
        x = self.state[1]
        y = self.state[3]
        return x, y

    #Calculates the total energy of the rocket, consists of the rockets potential energy and its kinetic energy
    def energy(self):
        x = self.state[1]
        y = self.state[3]
        vx = self.state[2]
        vy = self.state[4]
        m1 = o.mass_of_rocket(self.state[0])
        m2 = self.m_earth
        G = self.GravConst
        U = -G * m1 * m2 / sqrt(x ** 2 + (y + 12713.56e3 / 2) ** 2)
        K = m1 * (vx ** 2 + vy ** 2) ** 2 / 2
        return K + U

    #Gets the time that has elapsed since takeoff
    def time_elapsed(self):
        return self.state[0]

    #Approximating the next position and velocity, with the rocket_simulations-objects current stepsize
    def step(self, Win):
        s = np.zeros((6, self.dim))
        for i in range(0, 6):
            s[i, :] = self.F(Win + self.h * self.A[i, 0:i].dot(s[0:i, :]))
        Zout = Win + self.h * (self.B[0, :].dot(s))
        Wout = Win + self.h * (self.B[1, :].dot(s))
        E = np.linalg.norm(Wout - Zout, 2) / np.linalg.norm(Wout, 2)
        return Wout, E

    # Approximates the next position and velocity, using the RKF45-algorithm
    def safeStep(self, Win):
        Wout, E = self.step(Win)
        if not self.isErrorTolerated(E):
            self.adjustStep(E)
            Wout, E = self.step(Win)
        counter = 0
        while not self.isErrorTolerated(E):
            self.divideStepByTwo()
            Wout, E = self.step(Win)
            counter = counter + 1
            if counter > 10:
                sys.exit(-1)

        self.adjustStep(E)

        return Wout, E

    #checks if the error is tolerated by comparing the error produced by taking a step with the current step with
    #the chosen tolerance
    def isErrorTolerated(self, E):
        return E < self.tol
    #Adjusts stepsize. If the error is equal to 0 or machine eps, the stepsize is considered "too good"
    #and the stepsize is doubled, or else it is reduced
    def adjustStep(self, E):
        if E == 0:
            s = 2
        else:
            s = m.pow((self.tol * self.h) / (2 * E), 0.25)
        self.h = s * self.h
    #self-explanatory
    def divideStepByTwo(self):
        self.h = self.h / 2

    def setStepLength(self, stepLength):
        self.h = stepLength

    #The y'-function used when approximating the y-function
    def F(self, Y):
        px2 = 0
        py2 = 0
        px1 = Y[1]
        vx1 = Y[2]
        py1 = Y[3]
        vy1 = Y[4]
        m1 = o.mass_of_rocket(Y[0])
        m2 = self.m_earth
        G = self.GravConst
        f_d = o.f_d(Y[0], py1, vy1)
        f_rocket = o.f_rocket(Y[0])
        Gm2 = G * m2
        offset = 12713.56e3 / 2
        dist = np.sqrt((px2 - px1) ** 2 + (py2 - py1 - offset) ** 2)
        z = np.zeros(5)
        z[0] = 1
        z[1] = vx1
        z[2] = 0
        z[3] = vy1
        z[4] = ((Gm2 * m1) * (0 - py1 - offset) / (dist ** 3) + f_d + f_rocket) / m1
        return z

    #Approximating y up to the new time(current time + newT)
    def sim(self, newT):
        tEnd = self.state[0] + newT
        while self.state[0] + self.h < tEnd:
            self.state, E = self.safeStep(self.state)
        remaining_distance = tEnd - self.state[0]
        self.setStepLength(remaining_distance)
        self.state, E = self.step(self.state)
        self.h = self.originalH


rocket = Rocket_Simulation(5e-10, 1)
dt = 1. / 30.
fig = plot.figure(figsize=(7, 7))
ylim = 1e3
axes = fig.add_subplot(111, autoscale_on=True,
                       xlim=(-500, 500), ylim=(-ylim, ylim))

line1, = axes.plot([], [], 'o-y', lw=0.5)
line2, = axes.plot([], [], 'o-b', lw=2)
time_text = axes.text(0.02, 0.95, '', transform=axes.transAxes)
energy_text = axes.text(0.02, 0.90, '', transform=axes.transAxes)
height_text = axes.text(0.02, 0.85, '', transform=axes.transAxes)


def init():
    line1.set_data([], [])
    line2.set_data([], [])
    time_text.set_text('')
    energy_text.set_text('')
    height_text.set_text('')
    return line1, line2, time_text, energy_text, height_text


def animate(i):
    global rocket, dt, ylim
    rocket.sim(dt)
    line1.set_data(*rocket.position())
    line1.set_marker("d")
    line2.set_data([0.0, 0.0])
    line1.set_zorder(2)
    line2.set_zorder(2)
    line2.set_markersize(20)
    time_text.set_text('time = %.1f' % rocket.time_elapsed())
    energy_text.set_text('energy = %.3f J' % rocket.energy())
    height_text.set_text("height = %.1f m" % rocket.position()[1])
    if -ylim > rocket.state[3] or ylim < rocket.state[3]:
        ylim = ylim * 2
        axes.set_ylim(-ylim, ylim)
    return line1, line2, time_text, energy_text, height_text


t0 = time.time()
animate(1)
t1 = time.time()
delay = 1000 * dt - (t1 - t0)

anim = animation.FuncAnimation(fig,  # figure to plot in
                               animate,  # function that is called on each frame
                               frames=6000,  # total number of frames
                               interval=delay,  # time to wait between each frame.
                               repeat=True,
                               blit=True,
                               init_func=init  # initialization
                               )

# save the animation as an mp4.  This requires ffmpeg or mencoder to be
# installed.  The extra_args ensure that the x264 codec is used, so that
# the video can be embedded in html5.  You may need to adjust this for
# your system: for more information, see
# http://matplotlib.sourceforge.net/api/animation_api.html
# anim.save('orbit.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
plot.show()
