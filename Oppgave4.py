import numpy as np

#Calculates the mass of the rocket based on the elapsed time
def mass_of_rocket(t):
    m1 = 2280e3
    m2 = 480e3
    m3 = 123e3
    final_mass = 123000 - (109000*(5/17))
    if 0 <= t < 168:
        m = m1 + m2 + m3 + mdot(t) * t
    elif 168 <= t < 528:
        m = m2 + m3 + mdot(t) * (t - 168)
    elif 528 <= t < 678:
        m = m3 + mdot(t) * (t - 528)
    else:
        m = final_mass
    return m

#The drag works orthogonally on the area of the rocket, this area will be the area of the biggest part of the rocket
#this area changes over time as stages drops from the rocket when theyve burned up their fuel
def area_rocket(t):
    if 0 <= t < 168:
        a = np.pi * (10 / 2) ** 2
    elif 168 <= t < 528:
        a = np.pi * (10 / 2) ** 2
    else:
        a = np.pi * (6.604 / 2) ** 2
    return a

#Calculates how much fuel each stage has used
def mdot(t):
    if 0 <= t < 168:
        mdott = (2300000 - 131000) / 168
    elif 168 <= t < 528:
        mdott = 443000 / (528 - 168)
    elif 528 <= t < 678:
        mdott = (109000*(5/17)) / (678 - 528)
    else:
        mdott = 0
    return -mdott

#Each stage has a constant force, gets the force of a given stage
def f_rocket(t):
    f1 = 33000e3
    f2 = 4400e3
    f3 = 890000
    if 0 <= t < 168:
        f = f1
    elif 168 <= t < 528:
        f = f2
    elif 528 <= t < 678:
        f = f3
    else:
        f = 0

    return f

#gets the air density of the atmosphere which will be used in calculating the air resistance working on the rocket
def get_rho(h):
    if 0 <= h < 11000:
        t = 15.04 - .00649 * h
        p = 101.29 * ((t + 273.1) / 288.08) ** 5.256
    elif 11000 <= h < 25000:
        t = -56.56
        p = 22.65 * np.exp(1.73 - .000157 * h)
    elif 25000 <= h:
        t = -131.21 + .00299 * h
        p = 2.488 * ((t + 273.1) / 216.6) ** -11.388
    return p / (.2869 * (t + 273.1))

#Calculates the air resistance working on the rocket
def f_d(t, h, v):
    c_d = 0.521
    rho = get_rho(h)
    a = area_rocket(t)
    f_d = -(1 / 2) * c_d * rho * a * (v ** 2)
    return f_d
